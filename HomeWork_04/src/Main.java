
class MainTest {
    public static void main(String[] args) {

        assert Main.sumNumbers(1, 3) == 6;
        assert Main.sumNumbers(10, 5) == -1;

        Main.viewEven(new int[]{1, 2, 3, 4, 5, 6, 12, 112});

        int[] number = {9, 8, 7, 2, 3};
        assert Main.toInt(number) == 98723;

    }
}

class Main {

    public static int sumNumbers(int from, int to) {
        if (from > to) {
            return -1;
        }
        var sum = 0;
        for (int i = from; i <= to; i++) {
            sum += i;
        }
        return sum;
    }

    public static void viewEven(int[] numbers) {
        System.out.print("Четные элементы массива: ");
        for (int n : numbers) {
            if (n % 2 == 0) {
                System.out.print(n +" ");
            }
        }
    }

    public static int toInt(int[] number) {
        String num = "";
        for (int i : number){
            num = num + i;
        }
        return Integer.parseInt(num);
    }
}