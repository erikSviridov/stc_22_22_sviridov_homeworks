import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository{

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String,Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String colour = parts[2];
        String distance = parts[3];
        String price = parts[4];
        return new Car(number,model,colour,distance,price);
    };


    @Override
    public List<String> getNumbersByColour(String colour) {
        try{
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getColour().equals(colour))
                    .map(Car::getNumber)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public int numberOfModels(int from, int to) {
        try{
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getPrice() > from & car.getPrice() < to)
                    .map(Car::getModel)
                    .distinct()
                    .collect(Collectors.toList())
                    .size();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public String findCarColorWithMinPrice() {
        try{
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparing(Car::getPrice))
                    .get()
                    .getColour();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public double getAveragePrice(String model) {
        try{
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getModel().equals(model))
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}
