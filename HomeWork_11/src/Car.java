public class Car {
    private String number;
    private String model;
    private String colour;
    private int distance;
    private int price;

    public Car(String number,String model,String colour,String distance,String price){
        this.number = number;
        this.model = model;
        this.colour = colour;
        this.distance = Integer.parseInt(distance);
        this.price = Integer.parseInt(price);
    }


    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColour() {
        return colour;
    }

    public int getDistance() {
        return distance;
    }

    public int getPrice() {
        return price;
    }

}
