import java.util.List;

public interface CarsRepository {
    public List<String> getNumbersByColour(String colour);

    int numberOfModels(int from, int to);

    String findCarColorWithMinPrice();

    double getAveragePrice(String model);
}
