import java.util.List;

public class Main {
    public static void main(String[] args) {
        CarsRepositoryFileBasedImpl base = new CarsRepositoryFileBasedImpl("input.txt");
        List<String> carNumbers = base.getNumbersByColour("Black");

        assert carNumbers.size() == 2;
        assert carNumbers.get(0).equals("o001aa111");
        assert base.numberOfModels(700,800) == 0;
        assert base.findCarColorWithMinPrice().equals("Green");
        assert (int) base.getAveragePrice("Camry") == 54666;
    }
}
