import java.util.ArrayList;

public class OddNumbersPrintTask extends AbstractNumbersPrintTask {

    public OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public ArrayList<Integer> complete() {
        ArrayList<Integer> evenNumbers = new ArrayList<Integer>();
        for (int i = super.from; i < super.to; i++) {
            if (i % 2 != 0) {
                evenNumbers.add(i);
            }
        }
        return evenNumbers;
    }
}
