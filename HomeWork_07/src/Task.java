import java.util.ArrayList;

public interface Task {
    ArrayList<Integer> complete();
}
