import java.util.ArrayList;

public class Main {

    static void completeAllTasks(Task[] tasks){
        for(Task task : tasks){
                ArrayList<Integer> completedTask = task.complete();
                assert completedTask.size() == 2;
        }
    }

    public static void main(String[] args) {
        EvenNumbersPrintTask a = new EvenNumbersPrintTask(2,5);
        OddNumbersPrintTask b = new OddNumbersPrintTask(1,5);
        Task[] tasks = {a,b};
        completeAllTasks(tasks);
    }
}
