import java.util.ArrayList;

public abstract class AbstractNumbersPrintTask implements Task {
    protected int from;
    protected int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    abstract public ArrayList<Integer> complete();

}
