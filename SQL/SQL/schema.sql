drop table travel_history;
drop table if exists cars;
drop table if exists drivers;

create table drivers
(
    id bigserial primary key,
    first_name char(20) not null,
    last_name char(20) not null,
    telephone_number char(15) unique not null,
    experience smallint check ( experience >= 0 and experience <= 100 ) default 0,
    age smallint check ( age >= 0 and age <= 120 ),
    driving_license bool not null,
    license_category char(3),
    rating smallint check ( rating >= 0 and rating <= 5 ) default 5
);
create table cars
(
    id bigserial primary key,
    model char(20) not null,
    colour char(20) not null,
    number char(9) unique not null,
    driver_id bigserial,
    foreign key (driver_id) references drivers (id)
);

create table travel_history
(
    id bigserial primary key,
    driver_id bigserial not null,
    car_id bigserial not null,
    travel_date timestamp not null default current_timestamp,
    duration time not null,
    foreign key (driver_id) references drivers (id),
    foreign key (car_id) references cars (id)
);