insert into drivers (first_name, last_name, telephone_number, experience, age, driving_license, license_category)
values ('Василий', 'Иванов','+79602012766',20,54,true,'B'),
       ('Петр', 'Степанов', '+79213453457',10,48,true,'B'),
       ('Johns','Smith','+443456785434',5,28,true,'B'),
       ('Александр','Белознаменских','+79212123364',2,21,true,'B');

insert into drivers (first_name, last_name, telephone_number, age, driving_license)
values ('Максим','Соколов','+79992516783',18,false);

insert into cars (model, colour, number, driver_id)
values ('kia rio','white','h007qt178',1),
       ('ford focus','silver','k065mn178',2),
       ('mercedes benz', 'black', 'u067op178',3),
       ('bmw x5','black','t067ux178',3),
       ('kia rio','black','n576op178',5);

insert into travel_history (driver_id, car_id, duration)
values (1,1,'00:20:00');

insert into travel_history (driver_id, car_id, duration)
values (2,2,'00:44:42');

insert into travel_history (driver_id, car_id, duration)
values (3,3,'00:36:21');

insert into travel_history (driver_id, car_id, duration)
values (4,4,'00:14:23');

insert into travel_history (driver_id, car_id, duration)
values (5,5,'00:27:10')





