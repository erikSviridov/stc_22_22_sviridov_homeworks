import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductRepositoryFileBasedImpl implements ProductRepository {

    private final String fileName;

    public ProductRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        int id = Integer.parseInt(parts[0]);
        String name = parts[1];
        double price = Double.parseDouble(parts[2]);
        int count = Integer.parseInt(parts[3]);
        return new Product(id, name, price, count);
    };

    private static final Function<Product, String> productToStringMapper = currentProduct -> {
        return currentProduct.getId() + "|"
                + currentProduct.getName() + "|"
                + currentProduct.getPrice() + "|"
                + currentProduct.getCount() + "\n";
    };

    @Override
    public Product findById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId() == id)
                    .findFirst()
                    .get();
        } catch (FileNotFoundException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAllByTittleLike(String tittle) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getName().toLowerCase().contains(tittle.toLowerCase()))
                    .collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void update(Product product) {
        try {
            List<Product> products = new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(p -> p.getId() != product.getId())
                    .collect(Collectors.toList());
            products.add(product);
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            for (Product product1 : products) {
                String s = productToStringMapper.apply(product1);
                writer.write(s);
            }
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
