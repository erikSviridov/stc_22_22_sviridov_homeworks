public class Main {
    public static void main(String[] args) {
        ProductRepositoryFileBasedImpl base = new ProductRepositoryFileBasedImpl("product.txt");
        assert base.findById(1).getName().equals("Молоко");
        assert base.findAllByTittleLike("сЫРок").get(0).getName().equals("Сырок");

        Product milk = new Product(1, "Кефир", 39.8, 5);
        base.update(milk);
        assert base.findById(1).getName().equals("Кефир");
    }
}
