import java.util.List;

public interface ProductRepository {
    Product findById(Integer id);

    List<Product> findAllByTittleLike(String tittle);

    void update(Product product);
}
