import java.util.ArrayList;

public class ArrayTasksResolver {

    static int resolveTask(int[] array, ArrayTask task, int from, int to) {

        int result = task.resolve(array, from, to);

        System.out.print("Задан массив: ");
        for(int i : array) {
            System.out.print(i + " ");
        }
        System.out.println("\nВычисления проводятся от " + from + " до " + to + " элемента массива");
        System.out.println("Результат вычисления: " + task.resolve(array, from, to));

        return result;

    }
}
