import java.util.Arrays;

public class Test {
    public static void main(String[] args) {

        ArrayTask sumTask = (array,from,to) -> {
            int arraySum = 0;
            for (int i = from; i < to+1; i++) {
                arraySum += array[i];
            }
            return arraySum;
        };

        ArrayTask numberSumTask = (array,from,to) -> {

            int num = array[from];

            for (int i = from+1; i < to+1; i++) {
                if(num < array[i]) num = array[i];
            }

            int numSum = 0;

            while(num != 0){
                //Суммирование цифр числа
                numSum += (num % 10);
                num/=10;
            }
            return numSum;
        };

        int[] array = {12, 62, 4, 2, 100, 40, 56};
        int from = 1;
        int to = 3;

        assert ArrayTasksResolver.resolveTask(array,sumTask,from,to) == 68;
        assert ArrayTasksResolver.resolveTask(array,numberSumTask,from,to) == 8;


    }
}
