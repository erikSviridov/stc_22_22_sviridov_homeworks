import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MostFrequentWord {
    String words;
    HashMap<String, Integer> uniqueWords;

    MostFrequentWord(String words) {
        this.words = words;
        this.countWords();
    }

    void countWords() {
        String[] wordsArray = words.split(" ");
        uniqueWords = new HashMap<>();
        for (String i : wordsArray) {
            int x;
            Integer num = uniqueWords.get(i);
            if (num != null) {
                x = num + 1;
            } else {
                x = 1;
            }
            uniqueWords.put(i, x);
        }
    }

    String getWordAndFrueqncy() {
        int max = Collections.max(uniqueWords.values());
        String name = null;
        for(Map.Entry<String, Integer> entry: uniqueWords.entrySet()) {
            if(entry.getValue() == max) {
                name = entry.getKey();
                break;
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name);
        stringBuilder.append(" ");
        stringBuilder.append(max);
        return stringBuilder.toString();
    }
}
