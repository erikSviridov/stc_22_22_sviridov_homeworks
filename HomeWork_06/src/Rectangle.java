public class Rectangle extends Figures {
    protected int height;
    protected int width;

    public Rectangle(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public int perimeter() {
        return 2 * (this.height + this.width);
    }

    @Override
    public int square() {
        return this.height * this.width;
    }
}
