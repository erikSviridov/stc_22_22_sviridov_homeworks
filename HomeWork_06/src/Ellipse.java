public class Ellipse extends Figures {
    protected int majorAxis;
    protected int minorAxis;

    public Ellipse(int x, int y, int majorAxis, int minorAxis) {
        super(x, y);
        this.majorAxis = majorAxis;
        this.minorAxis = minorAxis;
    }

    public Ellipse(int majorAxis, int minorAxis) {
        this.majorAxis = majorAxis;
        this.minorAxis = minorAxis;
    }

    @Override
    public int perimeter() {
        return (int) (2 * 3.14 * Math.sqrt((this.majorAxis*this.majorAxis+this.minorAxis*this.minorAxis)/ 2));
    }

    @Override
    public int square() {
        return (int) 3.14 * this.majorAxis * this.minorAxis;
    }
}
