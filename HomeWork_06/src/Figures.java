public abstract class Figures {
    protected int[] coordinates;

    public Figures() {
        this.coordinates = new int[]{0, 0};
    }

    public Figures(int x, int y) {
        this.coordinates = new int[]{x, y};
    }

    public abstract int perimeter();

    public abstract int square();

    public void move(int x, int y) {
        this.coordinates[0] += x;
        this.coordinates[1] += y;
    }

    public int[] getCoordinates(){
        return this.coordinates;
    }

}
