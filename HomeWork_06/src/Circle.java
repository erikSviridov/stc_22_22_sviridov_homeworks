public class Circle extends Ellipse{

    public Circle(int x, int y, int radius) {
        super(x,y,radius,radius);
    }
    public Circle(int radius) {
        super(radius,radius);
    }

    @Override
    public int perimeter() {
        return (int) (2*3.14*super.majorAxis);
    }

}
