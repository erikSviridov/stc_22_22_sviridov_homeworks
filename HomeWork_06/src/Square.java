public class Square extends Rectangle {

    public Square(int x, int y, int side) {
        super(x,y,side,side);
    }

    public Square(int side) {
        super(side,side);
    }

}
