public class TestFigures {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2, 5);
        Ellipse ellipse = new Ellipse(3, 5);
        Square square = new Square(1);
        Circle circle = new Circle(5);

        Figures[] figures = {rectangle,ellipse,square,circle};
        testPerimeter(figures);
        testSquare(figures);

        circle.move(1,0);
        assert circle.getCoordinates()[0] == 1;

    }

    public static void testPerimeter(Figures[] figures){
        int[] answers = {14,25,4,31};
        for(byte i = 0; i < figures.length; i++){
            assert figures[i].perimeter() == answers[i];
        }
    }

    public static void testSquare(Figures[] figures){
        int[] answers = {10,45,1,75};
        for(byte i = 0; i < figures.length; i++){
            assert figures[i].square() == answers[i];
        }
    }

}
