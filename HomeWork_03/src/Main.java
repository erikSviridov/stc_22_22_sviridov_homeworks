import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int[] array;
        int numberOfLocalMin = 0;

        System.out.print("Введите последовательность чисел через пробел: ");

        String numbers = scanner.nextLine();
        String[] arrayOfnumbers = numbers.split(" ");

        array = new int[arrayOfnumbers.length];

        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(arrayOfnumbers[i]);
        }

        System.out.println("Размер массива: " + array.length);
        System.out.print("Массив содержит следующие элементы: ");

        int j = 0;
        for (int i : array) {
            if (j != array.length - 1) {
                System.out.print(i + ", ");
            } else {
                System.out.println(i);
            }
            j++;
        }
        if(array[0] < array[1] || array[array.length-2] > array[array.length-1])  numberOfLocalMin++;

        for (int i = 1; i < array.length - 1; i++) {
            if ((i == 0 && array[i] < array[i + 1]) || (i == array.length - 1 && array[i] < array[i - 1])) {
                numberOfLocalMin++;
            } else if (array[i] < array[i - 1] && array[i] < array[i + 1]) {
                numberOfLocalMin++;
            }
        }
        System.out.println("Количество локальных минимумов: " + numberOfLocalMin);
    }
}
