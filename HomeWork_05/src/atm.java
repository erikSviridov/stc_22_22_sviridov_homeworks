class tetstAtm{
    public static void main(String[] args){
        atm.maxSumInAtm = 60000;
        atm.putMoney(100);
        assert atm.getAmountOfMoneysInATm() == 100;
        assert atm.putMoney(100000) == 40000;
        assert atm.getAmountOfMoneysInATm() == 60100;

        atm.maxSumToGive = 40000;
        assert atm.getMoney(40000) == 40000;
        assert atm.getAmountOfMoneysInATm() == 20100;

        atm.maxSumToGive = 20000;
        assert atm.getMoney(25000) == 20000;
        assert atm.getAmountOfMoneysInATm() == 100;

        assert atm.getMoney(1000) == 100;
        assert atm.getAmountOfMoneysInATm() == 0;
    }
}

public class atm {
    private static int moneysInAtm;
    protected static int maxSumToGive;
    protected static int maxSumInAtm;
    private static int operationsNum = 0;

    public static int getMoney(int sum){
        if(sum > moneysInAtm ){
            moneysInAtm = 0;
            operationsNum++;
            return moneysInAtm;
        }
        if(sum >  maxSumToGive){
            moneysInAtm -= maxSumToGive;
            operationsNum++;
            return  maxSumToGive;
        }
        operationsNum++;
        return sum;
    }

    public static int putMoney(int sum){
        if(sum > maxSumInAtm) {
            moneysInAtm += maxSumInAtm;
            operationsNum++;
            return sum - maxSumInAtm;
        }
        moneysInAtm += sum;
        operationsNum++;
        return 0;
    }

    public static int getAmountOfMoneysInATm(){
        return moneysInAtm;
    }

}
