package ru.inno;


public class Main {

    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();

        Iterator<String> documentsIterator = documents.iterator();

        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }

        return mergedDocument.toString();
    }

    static void addDataToList(List<Integer> l) {
        l.add(8);
        l.add(10);
        l.add(11);
        l.add(13);
        l.add(11);
        l.add(15);
        l.add(-5);
    }

    static void testRemove(List<Integer> l) {
        l.remove(11);
        assert l.size() == 6;
        assert l.get(4) == 15;
    }

    static void testRemoveById(List<Integer> l) {
        l.removeAt(4);
        assert l.size() == 6;
        assert l.get(4) == 15;
    }

    public static void main(String[] args) {

        List<Integer> ArrayListForTestRemove = new ArrayList<>();
        addDataToList(ArrayListForTestRemove);
        testRemove(ArrayListForTestRemove);

        List<Integer> ArrayListForTestRemoveById = new ArrayList<>();
        addDataToList(ArrayListForTestRemoveById);
        testRemoveById(ArrayListForTestRemoveById);

        List<Integer> LinkedListForTestRemove = new LinkedList<>();
        addDataToList(LinkedListForTestRemove);
        testRemove(LinkedListForTestRemove);

        List<Integer> LinkedListForTestRemoveById = new LinkedList<>();
        addDataToList(LinkedListForTestRemoveById);
        testRemoveById(LinkedListForTestRemoveById);

    }

}